package com.cliftoncraig.libs.parse;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by cliff on 5/27/15.
 */
public class ResponseParseTest extends ResponseParse{
    ResponseParse responseParse;
    private InputStream response;

    @Before
    public void setUp() throws Exception {
        String testInputPath = "/response.json";
        URL testResource = getClass().getResource(testInputPath);
        Assert.assertNotNull("Should have test input for file at path " + testInputPath, testResource);
        this.response = testResource.openStream();
        this.responseParse = new ResponseParse();
    }

    @Test
    public void shouldParseResponse() throws Exception {
        final String[] urlStrings = {
                "https://farm4.staticflickr.com/3950/15021381983_ddfde648d4.jpg",
                "https://farm6.staticflickr.com/5598/15455564217_deda2c3bd3.jpg",
                "https://farm4.staticflickr.com/3947/15617829626_8bd2849ea0.jpg",
                "https://farm6.staticflickr.com/5597/15454897719_671a59abbc.jpg",
                "https://farm4.staticflickr.com/3943/15642377962_8865b93527.jpg",
                "https://farm4.staticflickr.com/3948/15617828576_fc20103d6d.jpg",
                "https://farm6.staticflickr.com/5599/15641549305_817407a0c1.jpg",
                "https://farm9.staticflickr.com/8880/17975208398_89836d7e7d.jpg",
                "https://farm9.staticflickr.com/8846/18136603576_fb4e9729be.jpg",
                "https://farm8.staticflickr.com/7743/17540519854_858919172a.jpg",
                "https://farm9.staticflickr.com/8781/17976833359_361a5d2234.jpg",
                "https://farm8.staticflickr.com/7764/17542351713_5c1e8ca2d8.jpg",
                "https://farm9.staticflickr.com/8887/17542267793_37df4cf1d5.jpg",
                "https://farm9.staticflickr.com/8849/18159295762_297c150466.jpg",
                "https://farm9.staticflickr.com/8893/17542211863_84a5bc3689.jpg",
                "https://farm8.staticflickr.com/7780/17974539288_d7f4975eca.jpg",
                "https://farm9.staticflickr.com/8871/17539853494_ed94203ec1.jpg",
                "https://farm8.staticflickr.com/7765/18163564631_ebeef3e437.jpg",
                "https://farm8.staticflickr.com/7788/17976201239_1bfe7cb86d.jpg",
                "https://farm9.staticflickr.com/8775/18162420475_a17b3b7c6e.jpg",
                "https://farm9.staticflickr.com/8809/17542516553_79cb57ab65.jpg"
        };
        List<URL> parsedURLs = responseParse.parse(response);
        List<String> actualURLs = new StringHelper().asString(parsedURLs);
        for(String eachExpectedUrl : Arrays.asList(urlStrings)) {
            assertTrue("Should contain " + eachExpectedUrl +"\n" + actualURLs, actualURLs.contains(eachExpectedUrl));
        }
    }

    boolean handlePhotosArrayWasInvoked;

    @Test
    public void shouldHandlePhotosArray() throws Exception {
        parse(response);
        assertTrue("handlePhotosCollection should be invoked", handlePhotosArrayWasInvoked);
    }

    @Override
    protected void handlePhotosArray(JSONArray photosArray) {
        handlePhotosArrayWasInvoked = true;
        for (int index = 0; index < photosArray.length(); index++) {
            JSONObject eachJsonObject;
            try { eachJsonObject = photosArray.getJSONObject(index); }
            catch (JSONException e) { throw new RuntimeException("error parsing photos array JSON at index " + index,e); }
            Map<String, String> photoEntry = super.createPhotoEntry(eachJsonObject);
            assertTrue("Photo entry should have " + SECRET_KEY, photoEntry.containsKey(SECRET_KEY));
            assertEquals("Should convert to a proper URL",
                    "https://farm" + photoEntry.get(FARM_KEY) + ".staticflickr.com/" + photoEntry.get(SERVER_KEY) + "/" + photoEntry.get(ID_KEY) + "_" + photoEntry.get(SECRET_KEY) + ".jpg",
                    responseParse.toURL(photoEntry));
            }
        return;
    }

}

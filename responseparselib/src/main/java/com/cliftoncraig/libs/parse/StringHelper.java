package com.cliftoncraig.libs.parse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cliff on 5/27/15.
 */
public class StringHelper {

    public String asString(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuffer result = new StringBuffer();
        try {
            for(String eachLine = reader.readLine(); eachLine!=null; eachLine = reader.readLine()) {
                result.append(eachLine);
            }
        } catch (IOException e) { throw new RuntimeException("Error readinf input stream.", e); }
        return result.toString();
    }

    public List<String> asString(List<URL> urls) {
        final List<String> strings = new ArrayList<>(urls.size());
        for(URL eachUrl : urls)
            strings.add(eachUrl.toString());
        return strings;
    }
}

package com.cliftoncraig.libs.parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cliff on 5/27/15.
 */
public class ResponseParse {

    public static final String FARM_KEY = "farm";
    public static final String SERVER_KEY = "server";
    public static final String ID_KEY = "id";
    public static final String SECRET_KEY = "secret";
    private List<URL> photoURLs;
    static final String flikrKey = "af47a934f43272042601c3f903959027";
    static final String FLIKR_SECRET = "a89b7f0319a988e6";

    public List<URL> parse(InputStream response) {
        photoURLs = new ArrayList<>();
        JSONObject jsonObject;
        try { jsonObject = new JSONObject(getJSONInputFromResponse(response)); }
        catch (JSONException e) { throw new RuntimeException("Could not parse JSON from response stream",e); }
        try { handlePhotosCollection(jsonObject.getJSONObject("photos")); }
        catch (JSONException e) { throw new RuntimeException("Could not find root photos collection"); }

        return photoURLs;
    }

    private JSONTokener getJSONInputFromResponse(InputStream response) {
        return new JSONTokener(new StringHelper().asString(response));
    }

    private JSONTokener deprecated_getJSONInputFromResponse(InputStream response) {
        return new JSONTokener(new InputStreamReader(response));
    }

    protected void handlePhotosCollection(JSONObject photos) {
        JSONArray photosArray;
        try { photosArray = photos.getJSONArray("photo"); }
        catch (JSONException e) { throw new RuntimeException("Could not find photos array"); }
        handlePhotosArray(photosArray);
    }

    protected void handlePhotosArray(JSONArray photosArray) {
        for (int index = 0; index < photosArray.length(); index++) {
            JSONObject eachJsonObject;
            try { eachJsonObject = photosArray.getJSONObject(index); }
            catch (JSONException e) { throw new RuntimeException("error parsing photos array JSON at index " + index,e); }
            final Map<String, String> photoEntry = createPhotoEntry(eachJsonObject);
            final URL url;
            try { url = new URL(toURL(photoEntry)); }
            catch (MalformedURLException e) { throw new RuntimeException("Invalid URL " + toURL(photoEntry),e); }
            this.photoURLs.add(url);
        }
        return;
    }

    protected Map<String, String> createPhotoEntry(JSONObject eachJsonObject) {
        Map<String, String> photoEntry = new HashMap<>();
        for(String expectedKey : new String[]{FARM_KEY, ID_KEY, SERVER_KEY, SECRET_KEY}) {
            try { photoEntry.put(expectedKey, eachJsonObject.getString(expectedKey)); }
            catch (JSONException e) { throw new RuntimeException("error parsing " + expectedKey + " in photos array JSON",e); }
        }
        return photoEntry;
    }

    public String toURL(Map<String, String> photoEntry) {
        return "https://farm" + photoEntry.get(FARM_KEY) + ".staticflickr.com/" + photoEntry.get(SERVER_KEY) +"/" + photoEntry.get(ID_KEY) + "_" + photoEntry.get(SECRET_KEY) + ".jpg";
    }
}

package com.cliftoncraig.apps.imagegallery;

import android.app.ListActivity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.cliftoncraig.libs.parse.ResponseParse;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class MainActivity extends ListActivity {

    public static final String FLICKR_SEARCH_SPEC = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=af47a934f43272042601c3f903959027&format=json&nojsoncallback=1&text=%s";
    private RowAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText searchField = (EditText) findViewById(R.id.searchField);
        handleEnterKey(searchField, new Runnable() {
            public void run() {
                doSearch(searchField.getText().toString());
            }
        });
        adapter = new RowAdapter(this);
        setListAdapter(adapter);
    }

    private ArrayList<Map<String, String>> createDataItems() {
        ArrayList<Map<String, String>> data = new ArrayList<>();
        data.add(makeItem(
                "image1", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg",
                "image2", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg",
                "image3", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg"
        ));
        data.add(makeItem(
                "image1", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg",
                "image2", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg",
                "image3", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg"
        ));
        data.add(makeItem(
                "image1", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg",
                "image2", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg",
                "image3", "https://tyrannyoftradition.files.wordpress.com/2012/05/cutest-kitten-hat-ever-13727-1238540322-17.jpg"
        ));
        return data;
    }

    private List<Map<String, String>> createDataItems(List<URL> parsedURLs) {
        ArrayList<Map<String, String>> data = new ArrayList<>();
        final int numberOfRows = parsedURLs.size() / 3;
        final Iterator<URL> urls = parsedURLs.iterator();
        for (int i = 0; i < numberOfRows; i++) {
            data.add(makeItem(
                    "image1", urls.next().toString(),
                    "image2", urls.next().toString(),
                    "image3", urls.next().toString()
            ));
        }
        int extra =parsedURLs.size() % 3;
        if (extra > 0) {
            data.add(makeItem(
                    "image1", extra > 0 ? urls.next().toString() : "",
                    "image2", extra > 1 ? urls.next().toString() : "",
                    "image3", extra > 2 ? urls.next().toString() : ""
            ));
        }
        return data;
    }

    private void handleEnterKey(final EditText searchField, final Runnable onEnterAction) {
        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && event.getAction() == KeyEvent.ACTION_DOWN) {
                    InputMethodManager inputMethod = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethod.hideSoftInputFromWindow(searchField.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    new Thread(onEnterAction).start();
                    return true;
                }
                return false;
            }
        });
    }

    private void doSearch(String searchString) {
//        URL[] imageUrls = getImages();
        String urlSpec = String.format(FLICKR_SEARCH_SPEC, searchString);
        URL url;
        try { url = new URL(urlSpec); }
        catch (MalformedURLException e) { throw new RuntimeException("Invalid search URL " + urlSpec, e); }
        InputStream responseStream;
        try { responseStream = url.openStream(); }
        catch (IOException e) { throw new RuntimeException("Could not load response for search URL " + urlSpec, e); }
//        logServerResponse(urlSpec, responseStream);
        adapter.setData(createDataItems(new ResponseParse().parse(responseStream)));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void logServerResponse(String urlSpec, InputStream responseStream) {
        String response = asString(responseStream);
        Log.i(getClass().getName(), "***Server response***");
        Log.i(getClass().getName(), urlSpec);
        Log.i(getClass().getName(), response);
        Log.i(getClass().getName(), "***Server response***");
    }

    private String asString(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuffer result = new StringBuffer();
        try {
            for(String eachLine = reader.readLine(); eachLine!=null; eachLine = reader.readLine()) {
                result.append(eachLine);
            }
        } catch (IOException e) { throw new RuntimeException("Error readinf input stream.", e); }
        return result.toString();
    }

    private HashMap<String, String> makeItem(String key1, String value1, String key2, String value2, String key3, String value3) {
        HashMap<String, String> item= new HashMap<>();
        item.put(key1, value1);
        item.put(key2, value2);
        item.put(key3, value3);
        return item;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

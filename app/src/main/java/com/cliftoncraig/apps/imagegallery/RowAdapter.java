package com.cliftoncraig.apps.imagegallery;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by cliff on 5/26/15.
 */
class RowAdapter extends BaseAdapter {
    private Context context;
    private List<Map<String, String>> data;

    public void setData(List<Map<String, String>> data) {
        this.data = data;
    }

    static class ViewHolder {
        ImageView image1;
        ImageView image2;
        ImageView image3;
        private final View view;
        private Context context;
        private AsyncTask<String, Void, Bitmap[]> task;
        private Map<String, String> item;

        public ViewHolder(Context context, ViewGroup parent) {
            this.context = context;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item, parent, false);
            this.image1 = (ImageView) view.findViewById(R.id.imageView1);
            this.image2 = (ImageView) view.findViewById(R.id.imageView2);
            this.image3 = (ImageView) view.findViewById(R.id.imageView3);
        }

        public View view() {
            return view;
        }

        public void setData(final Map<String, String> item) {
            if (this.item!=item) {
                updateImagePicasso(item);
                this.item = item;
            }
        }

        private void updateImagePicasso(Map<String, String> item) {
            if (isValidURL(item.get("image1"))) {
                loadUriToTarget(item.get("image1"), this.image1);
            }
            if (isValidURL(item.get("image2"))) {
                loadUriToTarget(item.get("image2"), this.image2);
            }
            if (isValidURL(item.get("image3"))) {
                loadUriToTarget(item.get("image3"), this.image3);
            }
        }

        private void loadUriToTarget(String uri, final ImageView target) {
            Picasso.with(context)
                    .load(uri)
                    .noFade()
                    .resize(100,100)
                    .centerCrop()
                    .into(target, new Callback() {
                        @Override
                        public void onSuccess() {
                            popView(target);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }

        private boolean isValidURL(String urlString) {
            try { new URL(urlString); }
            catch (MalformedURLException e) { return false; }
            return true;
        }

    }
    static Random random = new Random();

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static void popView(final View toAnimate) {
        toAnimate.setScaleX(0.2f);
        toAnimate.setScaleY(0.2f);
        toAnimate.animate()
                .setInterpolator(new OvershootInterpolator(5.0f))
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setStartDelay(random.nextInt(3) * 100)
                .start();
    }

    public RowAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder(context, parent);
            convertView = viewHolder.view();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.setData((Map<String, String>) getItem(position));
        return convertView;
    }
}
